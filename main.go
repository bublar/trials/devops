package main

import (
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

func main() {
	var (
		logger *zap.Logger
		err    error
	)

	switch os.Getenv("LOG_FMT") {
	case "json":
		logger, err = zap.NewProduction(zap.AddCaller())
	default:
		logger, err = zap.NewDevelopment(zap.AddCaller())
	}
	if err != nil {
		panic(err)
	}

	defer logger.Sync()
	sugar := logger.Sugar()

	connectionString := fmt.Sprintf("postgres://%s:%s@postgres/%s?sslmode=%s",
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("POSTGRES_SSL"))
	sugar.Infof("connecting to db with %s", connectionString)

	var db *sqlx.DB
	for i := 0; i < 5; i++ {
		time.Sleep(time.Second * time.Duration(i))
		db, err = sqlx.Open("pgx", connectionString)
		if err != nil {
			continue
		}
		err = db.Ping()
		if err != nil {
			continue
		}
	}

	if err != nil {
		sugar.Fatalf("Unable to connect to database: %v\n", err)
	}
	defer db.Close()

	if err = performMigrations(db.DB); err != nil {
		sugar.Fatalf("Unable to perform database migrations: %v\n", err)
	}
	sugar.Infof("migrations successfull")

	http.HandleFunc("/user", func(rw http.ResponseWriter, r *http.Request) {
		d := make([]byte, 16)
		if _, err := rand.Read(d); err != nil {
			sugar.Error(err)
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
		var id int
		if err := db.QueryRowx("INSERT INTO devops_test.users(name) VALUES ($1) RETURNING id;", hex.EncodeToString(d)).Scan(&id); err != nil {
			sugar.Error(err)
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		rw.WriteHeader(http.StatusOK)
		fmt.Fprintf(rw, "%d", id)
	})

	http.HandleFunc("/panic", func(rw http.ResponseWriter, r *http.Request) {
		sugar.Fatal("oh no")
	})

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM)

	go http.ListenAndServe(os.Getenv("HOSTPORT"), nil)

	sg := <-signalChan

	logger.Info("Received shut down signal", zap.String("end_signal", sg.String()))
}

func performMigrations(dbc *sql.DB) error {
	driver, err := postgres.WithInstance(dbc, &postgres.Config{})
	if err != nil {
		return err
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file://"+os.Getenv("MIGRATIONS_PATH"),
		"postgres",
		driver)
	if err != nil {
		return fmt.Errorf("migrate setup: %w", err)
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return fmt.Errorf("migrate up: %w", err)
	}
	return nil
}
