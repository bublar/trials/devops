# Trial Of DevOps

Welcome and thank you for accepting the DevOps trial of Bublar!

For clarification, questions or anything else please [send us an email!](mailto:incoming+bublar-trials-devops-20077078-issue-@incoming.gitlab.com)

## Instructions
Create a **private** clone of this repository, and once the assignment is complete share it with GitLab users @adriano.bublar and @aschwel.

This repository contains code for a HTTP server which has a dependency on Postgres.

The environment variables are:
* `LOG_FMT` (not required, `json` or defaults to human readable)
* `POSTGRES_USER` **required**
* `POSTGRES_PASSWORD` **required**
* `POSTGRES_DB` **required**
* `POSTGRES_SSL` **required** (`require` or `disable`)
* `MIGRATIONS_PATH` **required**

The server will attempt to run migrations on startup, and crashes in case of failure. It expects the migrations to be available on an accessible file system, inside the folder indicated with `MIGRATIONS_PATH`.

The server has two endpoints, which accept any HTTP Method:
* `/user` inserts a new user and returns the id, use to test the DB connection
* `/panic` triggers a crash

The server listens to `SIGTERM` to perform a graceful shutdown.

The server has a bug, the database hostport is hardcoded as `postgres`. For reasons unknown this cannot be fixed, and must be accommodated. 

## Assignment

The backend needs to run in 4 environments:
* **local** on machines of developers
* **development** inside a self hosted on-premises kubernetes 
* **staging** on AWS, used by internal testers and closed beta testers
* **production** on AWS, used by real users

You do **NOT** need to deploy the solution anywhere for this assignment.

Create a GitLab CI pipeline which builds a minimal size docker image and uploads it to your local Gitlab Package Registry. Use that image in the deploys.

Write a kubernetes configuration, or use an alternative system of configuration management to automate deployments to different environments.

Describe what a production deployment would look like, what services provided by AWS you would use and why. 

## For Your Consideration

Some developers do not work with servers, and require automated solutions for local development. What can you do to help them out so they can run the backend locally?

Is there a way to automate the AWS service configurations, and can we remain platform agnostic in case of a future transition to GKE or self-hosted?
