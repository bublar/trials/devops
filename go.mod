module gitlab.com/bublar/trials/devops

go 1.14

require (
	github.com/golang-migrate/migrate/v4 v4.11.0
	github.com/jackc/pgx/v4 v4.7.2
	github.com/jmoiron/sqlx v1.2.0
	go.uber.org/zap v1.15.0
)
